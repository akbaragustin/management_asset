<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
	Route::get('/logout', ['uses' =>'loginController@logout','as'=>'logout']);
    // Route::post('/api/v1/get-users', ['uses' =>'frontendController@api','as'=>'api']);
    // Route::get('/api/v1/get-items', ['uses' =>'frontendController@api_items','as'=>'api.items']);
// GUEST
Route::group(['middleware' => ['seller_guest']], function() {
	Route::get('/login', ['uses' =>'loginController@login','as'=>'login.login']);
	Route::post('/login-check', ['uses' =>'loginController@loginCheck','as'=>'login.check']);

});


 Route::group(['middleware' => 'seller_auth'], function() {
	Route::group(['namespace' => 'Admin','prefix' => 'admin'], function () {
		Route::get('/', ['uses' =>'homeController@index','as'=>'home.index']);
		Route::post('/schedule-ajax', ['uses' =>'homeController@indexAjax','as'=>'home.indexAjax']);
		Route::get('/schedule-ajax-search', ['uses' =>'homeController@searchAjax','as'=>'home.searchAjax']);
		Route::post('/schedule-ajax-save', ['uses' =>'homeController@saveAjax','as'=>'home.saveAjax']);
		Route::get('/schedule-ajax-delete/{id}', ['uses' =>'homeController@deleteAjax','as'=>'home.deleteAjax']);

		//jabatan
		Route::get('/jabatan', ['uses' =>'jabatanController@index','as'=>'jabatan.index']);
		Route::post('/jabatan-save', ['uses' =>'jabatanController@save','as'=>'jabatan.save']);
		Route::post('/jabatan-update', ['uses' =>'jabatanController@update','as'=>'jabatan.update']);
		Route::get('/jabatan-ajax', ['uses' =>'jabatanController@indexAjax','as'=>'jabatan.indexAjax']);
		Route::get('/jabatan-delete/{id}', ['uses' =>'jabatanController@delete','as'=>'jabatan.delete']);
		Route::get('/jabatan-edit/{id}', ['uses' =>'jabatanController@edit','as'=>'jabatan.edit']);
//Unit Kerja
		Route::get('/unit_kerja', ['uses' =>'unitKerjaController@index','as'=>'unit_kerja.index']);
		Route::post('/unit_kerja-save', ['uses' =>'unitKerjaController@save','as'=>'unit_kerja.save']);
		Route::post('/unit_kerja-update', ['uses' =>'unitKerjaController@update','as'=>'unit_kerja.update']);
		Route::get('/unit_kerja-ajax', ['uses' =>'unitKerjaController@indexAjax','as'=>'unit_kerja.indexAjax']);
		Route::get('/unit_kerja-delete/{id}', ['uses' =>'unitKerjaController@delete','as'=>'unit_kerja.delete']);
		Route::get('/unit_kerja-edit/{id}', ['uses' =>'unitKerjaController@edit','as'=>'unit_kerja.edit']);
//Master PIC
		Route::get('/master_pic', ['uses' =>'masterPicController@index','as'=>'master_pic.index']);
		Route::post('/master_pic-save', ['uses' =>'masterPicController@save','as'=>'master_pic.save']);
		Route::post('/master_pic-update', ['uses' =>'masterPicController@update','as'=>'master_pic.update']);
		Route::get('/master_pic-ajax', ['uses' =>'masterPicController@indexAjax','as'=>'master_pic.indexAjax']);
		Route::get('/master_pic-delete/{id}', ['uses' =>'masterPicController@delete','as'=>'master_pic.delete']);
		Route::get('/master_pic-edit/{id}', ['uses' =>'masterPicController@edit','as'=>'master_pic.edit']);

		//USERS
		Route::get('/users', ['uses' =>'usersController@index','as'=>'users.index']);
		Route::post('/users-save', ['uses' =>'usersController@save','as'=>'users.save']);
		Route::get('/users-ajax', ['uses' =>'usersController@indexAjax','as'=>'users.indexAjax']);
		Route::post('/users-update', ['uses' =>'usersController@update','as'=>'users.update']);
		Route::get('/users-delete/{id}', ['uses' =>'usersController@delete','as'=>'users.delete']);
		Route::get('/users-edit/{id}', ['uses' =>'usersController@edit','as'=>'users.edit']);

		//USERS ESELON
		Route::get('/users_eselon', ['uses' =>'usersController@index_eselon','as'=>'users_eselon.index']);
		Route::post('/users_eselon-save', ['uses' =>'usersController@save_eselon','as'=>'users_eselon.save']);
		Route::get('/users_eselon-ajax', ['uses' =>'usersController@indexAjax_eselon','as'=>'users_eselon.indexAjax']);
		Route::post('/users_eselon-update', ['uses' =>'usersController@update_eselon','as'=>'users_eselon.update']);
		Route::get('/users_eselon-delete/{id}', ['uses' =>'usersController@delete_eselon','as'=>'users_eselon.delete']);
		Route::get('/users_eselon-edit/{id}', ['uses' =>'usersController@edit_eselon','as'=>'users_eselon.edit']);

		//CONFIG MENU
		Route::get('/config/menu', ['uses' =>'configController@menu','as'=>'menu.index']);
		Route::post('/config/menu-save', ['uses' =>'configController@menuSave','as'=>'config.menuSave']);
		Route::post('/config/menu-update', ['uses' =>'configController@menuUpdate','as'=>'config.menuUpdate']);
		Route::get('/config/menu-view', ['uses' =>'configController@menuView','as'=>'config.menuView']);
		Route::get('/config/menu-edit/{id}', ['uses' =>'configController@menuEdit','as'=>'config.menuEdit']);
		Route::get('/config/menu-delete/{id}', ['uses' =>'configController@menuDelete','as'=>'config.menuDelete']);
		Route::get('/config/menu-viewIcon', ['uses' =>'configController@menuViewIcon','as'=>'config.menuViewIcon']);

		//CONFIG ROLE
		Route::get('/config/role', ['uses' =>'configController@menuRole','as'=>'role.index']);
		Route::get('/config/roleMenu', ['uses' =>'configController@reloadMenu','as'=>'config.reloadMenu']);
		Route::get('/config/roleMenu', ['uses' =>'configController@reloadMenu','as'=>'config.reloadMenu']);
		Route::post('/config/role-save', ['uses' =>'configController@roleSave','as'=>'config.roleSave']);

		//Edit USERS
		Route::get('/Editusers', ['uses' =>'usersController@editIndex','as'=>'Editusers.index']);
		Route::post('/users-updateEdit', ['uses' =>'usersController@updateEdit','as'=>'users.updateEdit']);

	});
 });
